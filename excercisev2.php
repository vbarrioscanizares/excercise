<?php 
	
const HRS_WEEK = 40;
	
class CVendeur {
    protected $name;
    protected $age;
	protected $position;
	protected $remuneration;
	protected $extra_remuneration = 0;
	protected $comission = 0;
	
	
	
    function __construct($name, $age, $position, $remuneration, $extra_remuneration = 0, $comission = 0) {
        $this->name = $name;
        $this->age  = $age;
		$this->position  = $position;
		$this->remuneration  = $remuneration;
		$this->extra_remuneration  = $extra_remuneration;
		$this->comission  = $comission;
	}
	
	
	function printSalaryByWeek($nbr_hrs) {	
	   $salary =  (int)($nbr_hrs / HRS_WEEK) * $this->remuneration;			
       vprintf("The salary for %s is $%s. \r\n", array($this->name, $salary ));
    }
	
	
	function printSalaryByHourWithExtra($nbr_hrs) {
       	   
		   if ( $nbr_hrs > HRS_WEEK){
						$salary = HRS_WEEK*$this->remuneration +($nbr_hrs	- HRS_WEEK) * $this->extra_remuneration;	
		   }else		$salary = $nbr_hrs * $this->remuneration;	
		   
		   		
       vprintf("The salary for %s is $%s. \r\n", array($this->name, $salary ));
    }
	
	
	function printSalaryFixWithComission($ammount_selled=0) {
       $salary = $this->remuneration + ($ammount_selled  *  ($this->comission / 100));			
       vprintf("The salary for %s is $%s. \r\n", array($this->name, $salary ));
    }
	
	
	
}




$karyne 	= new CVendeur("Karine",35,"commis de bureau",12,18);
$sylvain 	= new CVendeur("Sylvain",52,"Vendeur",750,0,10);
$marc 		= new CVendeur("Marc",45,"informaticien",1200,0);


$karyne->printSalaryByHourWithExtra(45); //570
$sylvain->printSalaryFixWithComission(10000); //1750
$marc->printSalaryByWeek(42); //1200


?>