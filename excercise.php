<?php 
	
const HRS_WEEK = 40;
	
class Employee {
    protected $name;
    protected $age;
	protected $position;
	protected $remuneration;
	protected $extra_remuneration = 0;
	protected $comission = 0;
	
	
	
    function __construct($name, $age, $position, $remuneration, $extra_remuneration = 0, $comission = 0) {
        $this->name = $name;
        $this->age  = $age;
		$this->position  = $position;
		$this->remuneration  = $remuneration;
		$this->extra_remuneration  = $extra_remuneration;
		$this->comission  = $comission;
	}
	
	
}

class CVendeur extends Employee{ //like Decorator, not really...
    protected $employee;

    public function __construct(Employee $employee) {
        $this->employee = $employee;
    }   
    
    function printSalary($nbr_hrs) {	
	   $salary =  (int)($nbr_hrs / HRS_WEEK) * $this->employee->remuneration;			
       vprintf("The salary for %s is $%s. \r\n", array($this->employee->name, $salary ));
    }
}


class CVendeurwithExtraSalary extends CVendeur { 
    protected $employee;

    public function __construct(Employee $employee) {
        $this->employee = $employee;
    }   
    
     function printSalary($nbr_hrs) {
       	   
		   if ( $nbr_hrs > HRS_WEEK){
						$salary = HRS_WEEK*$this->employee->remuneration +($nbr_hrs	- HRS_WEEK) * $this->employee->extra_remuneration;	
		   }else		$salary = $nbr_hrs * $this->employee->remuneration;	
		   
		   		
       vprintf("The salary for %s is $%s. \r\n", array($this->employee->name, $salary ));
    }
}



class CVendeurwithComission extends CVendeur {
    protected $employee;

    public function __construct(Employee $employee) {
        $this->employee = $employee;
    }   
    
     function printSalary($ammount_selled=0) {
       $salary = $this->employee->remuneration + ($ammount_selled  *  ($this->employee->comission / 100));			
       vprintf("The salary for %s is $%s. \r\n", array($this->employee->name, $salary ));
    }
}



$karyne 	= new Employee("Karine",35,"commis de bureau",12,18);
$sylvain 	= new Employee("Sylvain",52,"Vendeur",750,0,10);
$marc 		= new Employee("Marc",45,"informaticien",1200,0);

$karyne		= new CVendeurwithExtraSalary($karyne);
$sylvain	= new CVendeurwithComission($sylvain);
$marc		= new CVendeur($marc);

$karyne->printSalary(45); //570
$sylvain->printSalary(10000); //1750
$marc->printSalary(42); //1200


?>