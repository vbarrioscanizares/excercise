*** Please commit on GIT and share the respo with me ***

Tests programmation

La compagnie CTAC inc. a 3 employés :

Karine, 35 ans, commis de bureau, est rénuméré au taux horaire de 12$/heure et dépassé 40 heures, elle reçoit 18$/heure

Sylvain, 52 ans, vendeur, est payé à la semaine et reçoit un salaire de 750$/semaine + 10% des ses ventes hebdomadaire

Marc, 45 ans, informaticien, est payé 1200$/semaine

Faire une classe CVendeur en tenant compte du texte ci-haut.

Créer un programme qui appellera la classe CVendeur et qui calculera le salaire de Karine qui a travaillé 45 heure cette semaine, Sylvain qui a eu des ventes de 10000$ cette semaine et qui a travaillé 47 heures et finalement Marc qui a travaillé 42 heures cette semaine. 

Pas besoin d’interfaces graphiques, le but est simplement de valider votre compréhension 